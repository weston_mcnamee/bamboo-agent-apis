package com.edwardawebb.bamboo.agentapis.enums;

public enum UpgradeStance {

    LATEST("latest",true),STABLE("stable",false),TARGET("target",true);
    
    
    
    
    private static String KEY_PREFIX = "com.edwardawebb.mothermayaye.stance,descriptions.";
    
    private String key;
    private boolean isAllowed;
    
    
    UpgradeStance(String descriptionKey, boolean isAllowed){
        this.key = descriptionKey;
        this.isAllowed = isAllowed;
    }
    

    public String getDescriptionKey(){
        return KEY_PREFIX + key;
    }

    public String getDescription(){
        return KEY_PREFIX + key;
    }


    public boolean isAllowed() {
        return isAllowed;
    }
    
}
