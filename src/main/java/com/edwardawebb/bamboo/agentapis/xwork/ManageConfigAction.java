package com.edwardawebb.bamboo.agentapis.xwork;

import java.util.Arrays;
import java.util.List;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;



@SuppressWarnings("serial")
public class ManageConfigAction extends BambooActionSupport implements GlobalAdminSecurityAware {
    
   private List<UpgradeStance> upgradeStanceValues; 
    
    
    @Override
    public String doDefault() throws Exception {
        
        upgradeStanceValues = Arrays.asList(UpgradeStance.values());
            
            return SUCCESS;
    }


    public List<UpgradeStance> getUpgradeStanceValues() {
        return upgradeStanceValues;
    }


    
    
    
    
    
 
}
