package com.edwardawebb.bamboo.agentapis.xwork;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;


/**
 * This class delegates to {@BambooActionSupport} which returns "SUCCESS" which is linked to our template
 * in atlassian-plugin.xml
 * @author Eddie Webb
 *
 */
@SuppressWarnings("serial")
public class ManageApiTokensAction extends BambooActionSupport implements GlobalAdminSecurityAware {
 
   
    @Override
    public String doDefault() throws Exception {                  
        return SUCCESS;
    }

    
}
