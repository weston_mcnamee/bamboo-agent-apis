package com.edwardawebb.bamboo.agentapis.rest.agents.state;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.services.AgentService;

/**
 * A resource of message.
 */
@PublicApi
public class AgentState {
    
    private AgentService remoteAgentService;
    
    private AccessTokenService accessTokenService;
    
    
    
    

    public AgentState(AgentService remoteAgentService, AccessTokenService accessTokenService) {
        this.remoteAgentService = remoteAgentService;
        this.accessTokenService = accessTokenService;
    }

    @GET
    @AnonymousAllowed
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAgentsState(@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);

        if(token.isAllowedToRead()){
            return Response.ok(remoteAgentService.getAgents()).build();
        }else{
            return Response.status(Status.FORBIDDEN).build();
        }
    }

    /*
     * Methods that report state
     */
    @GET
    @AnonymousAllowed
    @Path("/{id}/state/text")
    public Response getBusyShorthandDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
       
       if(token.isAllowedToRead()){
           AgentStateModel agentModel = remoteAgentService.getStateFor(id);
           return Response.ok(String.format("ENABLED=%s%nBUSY=%s" ,agentModel.isEnabled(),agentModel.isBusy())).build();
       }else{
           return Response.status(Status.FORBIDDEN).build();
       }
        
    }

    @GET
    @AnonymousAllowed
    @Path("/{id}/state")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentState(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);   
    
        if(token.isAllowedToRead()){
           return Response.ok(remoteAgentService.getStateFor(id)).build();
        }else{
            return Response.status(Status.FORBIDDEN).build();
        }
    } 
    
    
    
    
    /*
     * Methods that update state
     *
     */
    @POST
    @AnonymousAllowed
    @Path("/{id}/state/enable")
    @Produces(MediaType.APPLICATION_JSON)
    public Response disableAgentDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);   
        
        if(token.isAllowedToChange()){
           return Response.ok(remoteAgentService.enable(id)).build();
        }else{
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
    @POST
    @AnonymousAllowed
    @Path("/{id}/state/disable")
    @Produces(MediaType.APPLICATION_JSON)
    public Response enableAgentDeprecated(@PathParam("id") long id,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return Response.status(Status.BAD_REQUEST).build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);   
        
        if(token.isAllowedToChange()){
           return Response.ok(remoteAgentService.disable(id)).build();
        }else{
            return Response.status(Status.FORBIDDEN).build();
        }
    }
    
}