package com.edwardawebb.bamboo.agentapis.rest.admin;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;

@Path("/admin-config")
public class AdminConfigurationEndpoint {

    private final UserManager userManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;

    public AdminConfigurationEndpoint(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,
            TransactionTemplate transactionTemplate) {
        this.userManager = userManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request) {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username)) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                return AdminConfiguration.getActiveConfig(pluginSettingsFactory);
            }
        })).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final AdminConfiguration config, @Context HttpServletRequest request)
    {
      String username = userManager.getRemoteUsername(request);
      if (username == null || !userManager.isSystemAdmin(username))
      {
        return Response.status(Status.UNAUTHORIZED).build();
      }

      transactionTemplate.execute(new TransactionCallback()
      {
        public Object doInTransaction()
        {
          PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
          pluginSettings.put(AdminConfiguration.class.getName() + ".stance", config.getStance().name());
          if(config.getStance().equals(UpgradeStance.LATEST)){
              pluginSettings.put(AdminConfiguration.class.getName()  +".targetVersion",UpgradeStance.LATEST.name());
          }else{
              String version = config.getTargetVersion();
              if(StringUtils.isBlank(version)){
                  version="LATEST";
              }
              pluginSettings.put(AdminConfiguration.class.getName()  +".targetVersion",version);
          }
          pluginSettings.put(AdminConfiguration.class.getName()  +".maxactive", Integer.toString(config.getMaxactive()));
          String version = config.getTargetVersion();
          
              
          return null;
        }
      });
      return Response.noContent().build();
    }

}
