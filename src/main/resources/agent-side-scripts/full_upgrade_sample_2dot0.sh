#!/bin/bash
#
#  This script should be triggered by crontab or similar scheduler on regular interval (i.e, Daily)
#   It will check in with the master server, and execute "UPGRADE_JOB" if allowed.
#
#    Provides as a sample file to Agent APIs for Bamboo it is free and open source.
#    Provided as is without warranty or liability.
#    If you were charged for this please report to authorities.
#
 
#
# Common Stuff
#      All processes will need the agent ID and uuid of an authorized token
#
 
# Agent ID can be hard coded, but is easily pulled from the running system
agentId=`cat ~/bamboo-agent-home/bamboo-agent.cfg.xml | grep -oPm1 "(?<=<id>)[^<]+"`
#check for required libraries/tools (optional)
command -v curl >/dev/null 2>&1 || { echo "Required tool 'curl' not found" ;exit 2; }
  
#grab uuid for this environment of local path (again this can ve hard coded depending on our use case)
# uuid=`cat /opt/bamboo/management/token.uuid`
uuid="7ba167aa-631c-4771-b197-fcb459cc7fd7"
# make a tmp dir for cookie jar and other random files we'll make
TMPDIRD=`mktemp -d /tmp/agentMaintenance.XXXXXX` || exit 1
# Bamboo api is hokey and even for "anonymous" access you need a cookie with the site info.  Just hit the homepage first
#  WARNING any other API calls fail without a valid cookie !!!
curl -k -c $TMPDIRD/cookies "$bambooUrl" > /dev/null 2>&1
  
  
#
#   Sample use case,    upgrade an agent
#
# EDIT THESE
bambooUrl="http://10.0.2.2:6990/bamboo" #domain, port and context without environment prefixes (currently mocked for testing. )
# AGGRESIVE SAMPLE TIMES, onlywiats 15 minutes.  Your's may wait as long as 1 day, week, etc.
SIBLING_PATIENCE_TIME=60 # will wait 1 minutes
SIBLING_PATIENCE_COUNT=15 # will repeat 15 times.
  
  
  
  
#
# Edit this to do whatever is considered a local upgrade.  It's called if server gives us permission
#
upgradeLocalAgent(){
 
    # save taskID to a file we can use after restart
    cp $TMPDIRD/state.txt ~/finishTaskOnStartup.txt
    # Use version to download from repo
    echo "downloading Agent version ${PVERSION}
    #curl "https//some.repo.you.have/agent-install.extension?v=${PVERSION}   # i.e. nexus/artifactory/fileshare rpm/tarball, etc.
    # shutdown local process
    echo "killing local bamboo agent"
    pkill -9 -f bamboo-agent-home
    # clear any previously set capabilities
    echo "purging old capabilities"
    curl -X DELETE -k -b $TMPDIRD/cookies "$bambooUrl/rest/agents/latest/$agentId/capabilities?uuid=${uuid}" -o $TMPDIRD/state.txt 2>/dev/null
  
  
    # call commands, chef, puppet, docker, REAL WORK, etc here.
    echo "beep bop, upgrading to version ${PVERSION} and saving task ID to file."
    ## this should include re-defining capabilities via bamboo-capabilities.properties
    ## clear any temporary space, old artifacts, etc.
    ## completely rebuild from an imaage in source (docker, etc)
    echo "Install complete, rebooting server"
  
}
  
  
  
#
#  DON'T CHANGE THIS STUFF
#  (unless you know why :) )
#
  
  
  
# Another function, skip reading this unles syou like loops
## we use function recursion, and track attempts to break out.
let attempts=1
checkBambooMaster(){
    curl -X POST -k -b $TMPDIRD/cookies  "$bambooUrl/rest/agents/latest/$agentId/maintenance?uuid=${uuid}"  -o $TMPDIRD/state.txt 2>/dev/null
  
    # MOnitor status until the agent is idle
    source $TMPDIRD/state.txt
    if [ "$PCODE" == "YES_CHILD" ]
    then
        #server says we can upgrade, make sure we are idle.
        echo "Master server says I can upgrade to version ${PVERSION}"
        if [ "$BUSY"  == "true" ]; then
            printf "\tAgent is still running a job, waiting ..\n"
            # while polling, and is still running, slee
            running=1
            while [ $running -eq 1 ]
            do
                sleep 60
                curl -k -b $TMPDIRD/cookies "$bambooUrl/rest/agents/latest/$agentId/state.text" -o $TMPDIRD/state.txt 2>/dev/null
                source $TMPDIRD/state.txt
  
                if [ "$BUSY"  == "false" ]; then
                    printf "\tYay, agent is now idle!\n"
                    break
                else
                    printf "\tstill busy..\n"
                fi
            done
        fi
    elif [ "$PCODE" == "WAIT_FOR_SIBLINGS" ]
    then
        # allowed to upgrae, but too many others are working right now, check back in a few
  
        echo "Master server wants me to wait, this is my $attempts attempt."
        echo "${PCODE}: ${PMESSAGE}"
        if [ $attempts -gt $SIBLING_PATIENCE_COUNT ]
        then
            echo "Siblings have exhausted my patience. INcrease wait times, offset cycles, or increase concurrency"
            exit 9
        fi
        let attempts+=1
        sleep $SIBLING_PATIENCE_TIME
        checkBambooMaster #will recurse back into this functuion
    elif [ "$PCODE" == "NO_CHILD" ]
    then
        echo "Master server says I can not upgrade now."
        echo "$PCODE: $PMESSAGE"
        exit 0
    elif [ "$PCODE" == "UH_OH" ]
    then
        echo "ERROR:  Master server is reporting an issue."
        echo "$PCODE: $PMESSAGE ,  existing Task ID: $TASK"
        exit 0
    else
  
        echo "ERROR:  I don't understand server response!"
        cat $TMPDIRD/state.txt
        exit 9
    fi
  
}
  
  
  
  
# disable agent in bamboo if allowed
echo "Requesting maintenance window from master server ${bambooUrl}"
checkBambooMaster
  
  
  
# assume success as function above exits on NOs. So call out heavy lifting function for maintenance.
echo "Agent was given permission, and is now disabled and idle, starting upgrade"
  
  
upgradeLocalAgent
 
##
##
## some time passes as bits are chewed and copied
##
##
 
##
## Server is now restarted either by reboot or call to java -jar atlassian-agent....
##
echo "starting java wrapper.."
#java -jar /vagrant/atlassian-bamboo-agent-installer-5.7.1.jar ${bambooUrl}/bamboo/agentServer/
# Report maintenance complete to master server
# grab task ID from task file
# SAMPLE:
#               PCODE=YES_CHILD
#               PMESSAGE="you may upgrade once idle"
#               PVERSION=1.2
#               ENABLED=false
#               BUSY=false
#               TASK=1
source ~/finishTaskOnStartup.txt
echo "Reporting Task : ${TASK} complete to ${bambooUrl}"
curl -X PUT -k -b $TMPDIRD/cookies  "$bambooUrl/rest/agents/latest/$agentId/maintenance/${TASK}/finish ?uuid=${uuid}"   2>/dev/null
 
# all done.   brand new or refreshed agent is back online, and others may not take thier turn.