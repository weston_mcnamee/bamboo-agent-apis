#!/bin/bash
#
#  This script should be triggered on server restart.
#  (or atleast after upgrades)
#  It should validate health before telling bamboo to re-enable.


#
#   CHANGE THIS STUFF
#
CORE_DOMAIN="localhost:6990/bamboo" #domain, port and context without environment prefixes.
## change case statement on line 41 if your prefixes are not dev-,test-,perf- or you don't use https.

UUID="7ba167aa-631c-4771-b197-fcb459cc7fd7"

SIBLING_PATIENCE_TIME=2 # will wait 10 seconds
SIBLING_PATIENCE_COUNT=9 # will repeat 9 times.








##### DONT TOUCH ######
if [ -s ~/finishTaskOnStartup.txt ]
then 
    echo "FIle indicating previous unfished task found. Will run smoke tests and re-enable agent"
    source ~/finishTaskOnStartup.txt
else
    echo "~/finishTaskOnStartup.txt not found, assuming no previous jobs in progress"
    exit 0;
fi



thisbox=`hostname`
envKey=${thisbox:4:1}
case "$envKey" in
    d)
    bambooUrl=https://dev-${CORE_DOMAIN}
    ;;
    t)
    bambooUrl=https://test-${CORE_DOMAIN}
    ;;
    f)
    bambooUrl=https://perf-${CORE_DOMAIN}
    ;;
    p)
    bambooUrl=https://${CORE_DOMAIN}
    ;;
    e)
#local testing
    bambooUrl=http://${CORE_DOMAIN}
    ;;
    *)
    echo "Unable to decipher environment from host $thisbox"
    exit 2
    ;;
esac




#check for required libraries/tools and setup tempdir
command -v curl >/dev/null 2>&1 || { echo "Required tool 'curl' not found" ;exit 2; }
scriptname=`basename $0`
TMPFILE=`mktemp -d /tmp/requestUpgrade.XXXXXX` || exit 1





# create cookie needed for rest endpoints
echo 
echo "Checking upgrade rules on master server ${bambooUrl}"
curl -k -c $TMPFILE/cookies "$bambooUrl" > /dev/null 2>&1


# reenable agent    
curl -H "uuid: ${UUID}" -X POST -k -b $TMPFILE/cookies "$bambooUrl/rest/agent/latest/mothermayi/finish/${TASK}.text" -o $TMPFILE/state.txt 2>/dev/null


# MOnitor status until the agent is idle
echo "Response from server: "
cat $TMPFILE/state.txt

