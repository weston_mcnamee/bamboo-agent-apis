package it.com.edwardawebb.bamboo.agentapis.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Before;

import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

public class AbstractEndpointTest {
    private static final String USERNAME="admin";
    private final static String PASSWORD="admin";
    
    protected ClientConfig clientConfig;
    protected  RestClient client;
    String agentBaseUrl = System.getProperty("baseurl") + "/rest/agents/1.0/";
    String configBaseUrl = System.getProperty("baseurl") + "/rest/agent-config/1.0/";

    String resourceUrlToken = configBaseUrl + "tokens";
    
     TokenResource readOnlyToken = new TokenResource("readOnly", true, false);
     TokenResource readAndWriteToken = new TokenResource("readOnly", true, true);

    @Before
    public void setup(){

        javax.ws.rs.core.Application app = new javax.ws.rs.core.Application() {
            public Set<Class<?>> getClasses() {
                Set<Class<?>> classes = new HashSet<Class<?>>();
                classes.add(JacksonJaxbJsonProvider.class);
                return classes;
            }

        };
        //create auth handler
        clientConfig = new ClientConfig();
        clientConfig.applications(app);
        BasicAuthSecurityHandler basicAuthSecurityHandler = new BasicAuthSecurityHandler();
        basicAuthSecurityHandler.setUserName(USERNAME);
        basicAuthSecurityHandler.setPassword(PASSWORD); 
        clientConfig.handlers(basicAuthSecurityHandler);
        //create client usin auth   
        client = new RestClient(clientConfig);
      
    }
    

}
